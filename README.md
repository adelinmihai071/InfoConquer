# InfoConquer Setup Instructions

## 1. Install NodeJS

Visit [Node.js Download Page](https://nodejs.org/en/download) and install NodeJS.

## 2. Install the dependencies

Run the following command in both the server and client terminals to install dependencies:

```bash
npm install
```
## 3. Create a `.env` file

Create a `.env` file in the project directory and include the following variables:

```env
PORT=YOUR_SERVER_PORT
MONGO_DB=YOUR_MONGODB_LINK
SECRET=YOUR_JWT_SECRET
```
Replace YOUR_SERVER_PORT, YOUR_MONGODB_LINK, and YOUR_JWT_SECRET with your desired values.

**4. Run the application**

Execute the following command in both the client and server terminals:

```bash
npm run start
```
