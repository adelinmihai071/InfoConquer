const jwt = require('jsonwebtoken');

const authRequire = async(req, res, next) => {
    const {authorization} = req.header;
    if(!authorization){
        res.status(401).json({error: "NO AUTHORIZATION"});

        const token = authorization.split(' ')[1];

        try {
            const {_id} = jwt.verify(token, process.env.SECRET);
            req.user = await User.findOne({_id}).select('_id');
            next()
        } catch (error) {
            res.status(401).json({error: 'Request not authorized'});
        }
    }
}

module.exports = authRequire;