const mongoose = require('mongoose');

const problemSchema = new mongoose.Schema({
    creator_username: {
        type: String,
        required: true
    },
    titlu: {
        type: String,
        required: true,
        unique: true
    },
    cerinta: {
        type: String,
        required: true
    },
    tip: {
        type: String,
        required: true
    },
    tags: {
        type: Array,
        required: true
    },
    dificultate: {
        type: Array,
        required: true
    },
    input: {
        type: String,
        required: true
    },
    output: {
        type: String,
        required: true
    },
    inputFile: {
        type: String,
        required: true
    },    
    outputFile: {
        type: String,
        required: true
    },
    restrictii: {
        type: String,
        required: true
    },
    timp: {
        type: String,
        required: true
    },
    exemple: {
        type: Array,
        required: false
    },
    teste: {
        type: Array,
        required: true
    },
    limitaMemorie: {
        type: String,
    },
    clasa:{
        type: String
    },
}, {timestamps: true})
problemSchema.statics.add = async function(creator_username, titlu, cerinta, tip, tags, dificultate, input, output, inputFile, outputFile, restrictii, timp, exemple, teste, limitaMemorie, clasa){
    if(!creator_username || !titlu || !limitaMemorie || !cerinta || !tip || !tags || !dificultate || !input || !output || !inputFile || !outputFile || !restrictii || !timp || !teste || !clasa){
       throw Error('All fields must be filled')
   }
    const newProblem = await this.create({creator_username, titlu, cerinta, tip, tags, exemple, dificultate, input, output, inputFile, outputFile, restrictii, timp, exemple, teste, limitaMemorie, clasa})
    return newProblem
}
module.exports = new mongoose.model('Problem', problemSchema);