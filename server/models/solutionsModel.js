const mongoose = require('mongoose');

const solutionSchema = new mongoose.Schema({
    username:{
        type: String,
        required: true
    },
    solutie:{
        type: String,
        required: true
    },
    punctaj:{
        type: Number,
        required: true
    },
    titluProblema:{
        type: String,
        required: true
    },
    memorie:{
        type: Number,
        required: true
    },
    timp:{
        type: Number, 
        required: true
    },
    data:{
        type: String,
        required: true
    }
    ,
    limbaj:{
        type: String,
        required: true
    }
})
solutionSchema.statics.add = async function(username, solutie, punctaj, titluProblema, memorie, timp, data, limbaj){
    const newSolution = await this.create({username, solutie, punctaj, titluProblema, memorie, timp, data, limbaj})
    return newSolution;
}
module.exports = new mongoose.model('solutionSchema', solutionSchema);