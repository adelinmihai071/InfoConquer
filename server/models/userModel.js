const mongoose = require('mongoose');
const validator = require('validator');
const bcrypt = require('bcrypt');

const userSchema = new mongoose.Schema({
    username: {
        type: String,
        required: true,
        unique: true
    },
    email: {
        type: String, 
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    admin: {
        type: Boolean,
        required: true
    }
}, {timestamps: true})

userSchema.statics.signin = async function(email, password) {
    if(!email || !password){
        throw Error('All sign in fields must be filled');
    }
    const user = await this.findOne({email});
    if(!user){
        throw Error('User not found')
    }
    const matchPasswords = await bcrypt.compare(password, user.password)
    if(!matchPasswords){
        throw Error('Incorrect password');
    }
    return user;
}
userSchema.statics.signup = async function(email, password, username) {
    if(!email || !password || !username){
        throw Error('All signup fields must be filled');
    }
    const existingUsername = await this.findOne({username});
    if(existingUsername){
        throw Error('Username already in use')
    }
    if(!validator.isEmail(email)){
        throw Error('Enter a valid email');
    }
    if(!validator.isStrongPassword(password)){
        throw Error('Enter a stronger password');
    }
    const existingUser = await this.findOne({email});
    if(existingUser){
        throw Error('User with this email already exists');
    }
    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(password, salt);
    const newUser = await this.create({email, password:hashedPassword, username, admin:false})
    return newUser;
}
module.exports = mongoose.model('user', userSchema);