const SolutionSchema = require('../models/solutionsModel')
const postSolution = async(req, res) => {
    const {username, solutie, punctaj, titluProblema, memorie, timp, data, limbaj} = req.body;
    try {
        const newSolution = await SolutionSchema.add(
            username,
            solutie, 
            punctaj,
            titluProblema,
            memorie,
            timp,
            data,
            limbaj
        )
        res.status(200).json({newSolution})
    } catch (error) {
        res.status(500).json({ error: error.message })
    }
}
module.exports = {postSolution}