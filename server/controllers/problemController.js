const Problem = require('../models/ProblemModel');

const publish = async (req, res) => {
    const { creator_username, titlu, cerinta, tip, tags, dificultate, input, output, inputFile, outputFile, restrictii, exemple, timp, teste, limitaMemorie, clasa } = req.body;
    try {
        const newProblem = await Problem.add(
            creator_username,
            titlu,
            cerinta,
            tip,
            tags,
            dificultate,
            input,
            output,
            inputFile,
            outputFile,
            restrictii,
            timp,
            exemple,
            teste,
            limitaMemorie,
            clasa
        );
        res.status(200).json({ newProblem });
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};
module.exports = { publish };