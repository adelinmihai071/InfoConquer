const User = require('../models/userModel');
const jwt = require('jsonwebtoken');

const generateToken = (_id) => {
    return jwt.sign({_id}, process.env.SECRET, {expiresIn: '3h'})
}

const signin = async(req, res) => {
    const {email, password} = req.body;
    try {
        const user = await User.signin(email, password);
        const token = generateToken(user._id);
        const username = user.username;
        res.status(200).json({email, token, username});
    } catch (error) {
        res.status(500).json({error: error.message});
    } 
}
const signup = async(req, res) => {
    const {email, password, username} = req.body;
    try {
        const user = await User.signup(email, password, username);
        const token = generateToken(user._id);
        res.status(200).json({email, token, username})
    } catch (error) {
        res.status(500).json({error: error.message})
    }
}

module.exports = {signin, signup}