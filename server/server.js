const express = require('express');
const cors = require('cors')
const app = express();
const fs = require('fs')
const vm = require('vm');
app.use(express.json());
app.use(cors())
const { execSync } = require('child_process');
require('dotenv').config();
const Problem = require('./models/ProblemModel')
const mongoose = require('mongoose');
const userRoutes = require('./routes/userRoutes');
const authRequire = require('./middlewares/authRequire');
const problemRoute = require('./routes/problemRoute');
const userModel = require('./models/userModel');
const problemModel = require('./models/ProblemModel');
const solutionsModel = require('./models/solutionsModel')
app.use(
    cors({
        origin: ['http://localhost:3000/', 'https://infoconquer.netlify.app', 'http://192.168.1.15:3000']
    })
)
app.use('/user', userRoutes);
app.use('/problem', problemRoute)
////////////////////////////////////////////////////
app.get('/getProblems', async(req, res) => {
    try {
        const problems = await Problem.find({})
        res.status(200).json({problems})
    } catch (error) {
        res.status(500).json({ error: 'Error fetching problems' });
    }
})
/////////////////////////////////////////////////////
app.post('/python', (req, res) => {
    const pythonCode = req.body.code;
    const tests = req.body.teste;
    fs.writeFileSync('test.py', pythonCode);
    const testeCompilate = [];
    for (const testCase of tests) {
        try {
            const startTime = new Date().getTime();
            const output = execSync(`echo "${testCase.input}" | python3 test.py`, {
                encoding: 'utf-8'
            })
            const endTime = new Date().getTime();
            const executionTime = endTime - startTime;

            const success = output.trim() === testCase.output.trim();
            const puncte = success ? testCase.points : 0;
            testeCompilate.push({
                success,
                puncte,
                timpExecutie: executionTime
            });
            } catch (error) {
                testeCompilate.push({
                    success: false,
                    puncte: 0,
                    timpExecutie: 0,
                    error: error.stderr.split('\n')
                });
            }
        }
        res.json({ testeCompilate });
});

app.post('/cpp', (req, res) => {
    const cppcode = req.body.code
    const teste = req.body.teste
    const testeCompilate = [];
    fs.writeFileSync('test.cpp', cppcode); 
    for (const testCase of teste) {
        try {
            const compilationResult = execSync('g++ test.cpp -o ./test 2>&1', {
                encoding: 'utf-8'
            });
            if (compilationResult.trim() !== '') {
                testeCompilate.push({
                    success: false,
                    puncte: 0,
                    timpExecutie: 0,
                    error: compilationResult 
                });
                continue;
            }
            const startTime = new Date().getTime();
            const outputTest = execSync(`echo "${testCase.input}" | ./test`, {
                        encoding: 'utf-8'
            });
            const endTime = new Date().getTime();
            const executionTime = endTime - startTime;

            const success = outputTest.trim() === testCase.output.trim();
            const puncte = success ? testCase.points : 0;

            testeCompilate.push({
                    success,
                    puncte,
                    timpExecutie: executionTime
            });
        } catch (error) {
            const errorLines = error.stdout.split('\n');
                testeCompilate.push({
                success: false,
                puncte: 0,
                timpExecutie: 0,
                error: errorLines
            });
        }
    }
    res.json({ testeCompilate });
});
app.post('/cs', (req, res) => {
    const csharpCode = req.body.code;
    const tests = req.body.teste;
    const testeCompilate = [];
    fs.writeFileSync('test.cs', csharpCode);
    for (const testCase of tests) {
        try {
            const startTime = new Date().getTime();
            const compileResult = execSync(`node test.js`, {
                input: testCase.input,
                encoding: 'utf-8'
            });
            const endTime = new Date().getTime();
            const executionTime = endTime - startTime;
            const success = compileResult.trim() === testCase.output.trim();
            const puncte = success ? testCase.points : 0;
            testeCompilate.push({
                success,
                puncte,
                timpExecutie: executionTime
            });
        } catch (error) {
            const errorLines = error.stdout.split('\n');
            testeCompilate.push({
                success: false,
                puncte: 0,
                timpExecutie: 0,
                error: errorLines
            });
        }
    }
    res.json({ testeCompilate });
});
app.post('/javascript', (req, res) => {
    const jsCode = req.body.code;
    const tests = req.body.teste;
    fs.writeFileSync('test.js', jsCode);
    const testeCompilate = [];
    for(const testCase of tests){
        try{
            const startTime = new Date().getTime();
            const compileResult = execSync(`node test.js`, {
                input: testCase.input,
                encoding: 'utf-8'
            })
            const endTime = new Date().getTime();
            const executionTime = endTime - startTime;
            const success = compileResult.trim() === testCase.output.trim();
            const puncte = success ? testCase.points : 0;
            testeCompilate.push({
                success,
                puncte,
                timpExecutie: executionTime
            });
        }catch(error){
            const errorLines = error.stdout.split('\n');
            testeCompilate.push({
                success: false,
                puncte: 0,
                timpExecutie: 0,
                error: errorLines
            });
        }
    }
    res.json({testeCompilate})
});
app.post('/c', (req, res) => {
    const ccode = req.body.code
    const type = req.body.type
    const input = req.body.terminalInput;
    fs.writeFileSync('test.c', ccode)
    switch(type){
        case 'run':
            execSync('g++ test.c -o test', { stdio: 'inherit' });
            const output = execSync(`echo "${input}" | ./test`, { encoding: 'utf-8' });
            res.json({output})
    }
})
app.post('/java', (req, res) => {
    const javaCode = req.body.code;
    const type = req.body.type;
    const input = req.body.terminalInput;
    const titlu = req.body.titlu
    fs.writeFileSync(`${titlu}.java`, javaCode);

    switch (type) {
        case 'run':
            execSync(`javac ${titlu}.java`, { stdio: 'inherit' });
            try {
                const output = execSync(`echo "${input}" | java ${titlu}`, { encoding: 'utf-8' });
                res.json({ output });
            } catch (error) {
                console.error(error);
                res.status(500).json({ error: error.message });
            }
            break;
    }
});
app.get('/getUsers', async(req, res) => {
    try{
        const user = await userModel.find({})
        res.status(200).json({user})
    }catch(error){
        res.status(500).json({error})
    }
})
mongoose.connect(process.env.MONGO_DB).then(() => {
    app.listen(process.env.PORT, () => {
        console.log('App started and database connected');
    })
}).catch((error) => {
    console.log(error);
})