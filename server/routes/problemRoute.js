const express = require('express');
const router = express.Router();
const {publish} = require('../controllers/problemController')
const {postSolution} = require("../controllers/solutionController")
const solutionSchema = require('../models/solutionsModel');
router.post('/add', publish)
router.post('/postSolution', postSolution);
router.get('/getSolutions/:id', async(req, res) => {
    try {
        const id = req.params.id
        const solutions = await solutionSchema.find({titluProblema: id});
        res.json({solutions});
    } catch (error) {
        res.status(500).json({error: 'Internal server error'});
    }
})
router.get('/getSolutii', async(req, res) => {
    try {
        const solutions = await solutionSchema.find({});
        res.status(200).json({solutions});
    } catch (error) {
        res.status(500).json({error: 'Internal error error'})
    }
})
module.exports = router;