import Typewriter from 'typewriter-effect'
import { useState } from 'react';
const Home = () => {
    const programmingKeywords = [
        'variable', 'function', 'class', 'method', 'loop', 'array', 'string', 'integer',
        'boolean', 'float', 'if', 'else', 'switch', 'case', 'break', 'continue', 'return',
        'import', 'export', 'const', 'let', 'for', 'while', 'true', 'false', 'null',
        'undefined', 'this', 'new', 'instanceof', 'try', 'catch', 'throw', 'finally',
        'await', 'async', 'function*', 'yield', 'extends', 'super', 'constructor',
        'static', 'interface', 'implements', 'package', 'protected', 'private', 'public',
        'enum', 'export default', 'import { ... } from \'module\'', 'require',
        'module.exports'
      ];
      
    const getRandomRotation = () => {
        return Math.floor(Math.random() * 180)
    }
    const getRandomLeft = () => {
        return Math.floor(Math.random() * 100)
    }
    const getRandomTop = () => {
        return Math.floor(Math.random() * 90)
    }
    return ( 
        <div className="home">
            <div id="logo-home">
                <div className="logoHomepage">
{/*                     <Tabs onChange={handleChange} value={value} style={{opacity: '100%'}}>
                        <div style={{opacity: '100%'}}>
                            <Tab style={{backgroundColor: '#313131', opacity: '100%'}} label='Javascript'></Tab>
                            <Tab style={{backgroundColor: '#313131', opacity: '100%'}} label='C#'></Tab>
                            <Tab style={{backgroundColor: '#313131', opacity: '100%'}}label='Python'></Tab>
                        </div>
                    </Tabs>
                        <div tabIndex={0}>
                            <CodeMirror
                                height='400px' 
                                value={code}
                                theme={xcodeLight}
                                extensions={python()}
                                inputMode={python}
                                width='100%'
                                maxWidth='auto'
                            />
                        </div> */}
{/*                         <div tabIndex={1}>
                            <CodeMirror
                                height='400px' 
                                value={'asd'}
                                theme={xcodeLight}
                                extensions={python()}
                                inputMode={python}
                                width='100%'
                                maxWidth='auto'
                            />
                        </div>
                        <div tabIndex={2}>
                            <CodeMirror
                                height='400px' 
                                value={''}
                                theme={xcodeLight}
                                extensions={python()}
                                inputMode={python}
                                width='100%'
                                maxWidth='auto'
                            />
                        </div> */}
             </div> 
                <div className='typewriter'>
                    <Typewriter
                        options={{
                            loop: true
                        }}
                        onInit={(typewriter) => {
                            typewriter
                            .pauseFor(50)
                            .typeString("InfoConquer")
                            .pauseFor(300)
                            .deleteAll()
                            .typeString("Homepage")
                            .pauseFor(1000)
                            .start();
                        }}
                    />
                    <div className="homepage-introduction">
                        What can you conquer?
                    </div>
                </div>
            </div> 
            {programmingKeywords.map((keyword) => (
                <div className="keyword" style={{position: 'absolute', left: `${getRandomLeft().toString()}%`, transform: `rotate(${getRandomRotation().toString()}deg)`, top: `${getRandomTop().toString()}%`}}>{keyword }</div>
            ))}
        </div>
     );
}
 
export default Home;
