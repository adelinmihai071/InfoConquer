import {Link} from 'react-router-dom'
const Problems = () => {
    return (  
        <div className="problems-wrapper" style={{backgroundColor: '#121212'}}>
            <Link to='/add' className='problem-link'>
                <h1>Posteaza o problema</h1>
            </Link>
            <div className="problems">
                <div>
                    <div className="concursuri-title">Concursuri</div>
                        <div className="concursuri-scolare">
                                <Link to='/oji'>OJI</Link>
                                <Link to='/oni'>ONI</Link>
                                <Link to='/lotulnational'>Lotul National</Link>
                                <Link to='/omi-iasi'>Olimpiada municipala de informatica Iasi</Link>
                                <Link to='/jboi'>Olimpiada balcanica de informatica pentru juniori</Link>
                                <Link to='/info-oltenia'>Info-Oltenia</Link>
                        </div>
                </div>
                <div>
                    <div className="concursuri-admitere-title">Concursuri admitere</div>
                    <div className="concursuri-admitere">
                        <Link to='/unibuc'>UNIBUC</Link>
                        <Link to='/ubb'>UBB</Link>
                        <Link to='/uaic'>UAIC</Link>
                        <Link to='/upb'>UPB</Link>
                        <Link to='/uvt'>UVT</Link>
                        <Link to='tuiasi'>UTGA</Link>
                    </div>
                </div>
            </div>
        </div>
    );
}
 
export default Problems;