import { useState } from 'react';
import {useSignin} from '../hooks/useSIgnin'
import TextField from '@mui/material/TextField'
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import InputAdornment from '@mui/material/InputAdornment';
import { IconButton } from "@mui/material";
import MarkEmailReadIcon from '@mui/icons-material/MarkEmailRead';
import GitHubIcon from '@mui/icons-material/GitHub';
import GoogleIcon from '@mui/icons-material/Google';
import { Button, Dialog } from '@mui/material';
import {Link} from 'react-router-dom'
import CheckIcon from '@mui/icons-material/Check';
import { useSignup } from "../hooks/useSignup";
import Slide from '@mui/material/Slide';

const Signin = () => {
    const [username, setUsername] = useState('');
    const handleClickShowPassword = () => setShowPassword((show) => !show);
    const [open, setOpen] = useState(false)
    const [open2, setOpen2] = useState(false)
    const [showPassword, setShowPassword] = useState(false)
    const {signin, error, loading} = useSignin();
    const {signup, errorSignup, loadingSignup} = useSignup();
    const [password, setPassword] = useState('');
    const [email, setEmail] = useState('');
    const handleSubmit = async(e) => {
        e.preventDefault();
        await signin(email.trim(), password.trim());
    }
    const handleSubmitSignup = async(e) => {
        e.preventDefault();
        await signup(email, password, username);
    }
    const handleClose = () => {
        setOpen(false);
    };
    const handleOpen = () => {
        setOpen(true)
    }
    const handleDialogSignUpClick = () => {
        setOpen2(true)
    }
    const handleDialogSignUpClose = () => {
        setOpen2(false)
    }
    return ( 
        <>
        <Button onClick={handleOpen} style={{marginRight: '20px'}}>
        <svg style={{fill: 'white', marginRight: '10px'}} xmlns="http://www.w3.org/2000/svg" height="1em" viewBox="0 0 448 512"><path d="M224 256A128 128 0 1 0 224 0a128 128 0 1 0 0 256zm-45.7 48C79.8 304 0 383.8 0 482.3C0 498.7 13.3 512 29.7 512H418.3c16.4 0 29.7-13.3 29.7-29.7C448 383.8 368.2 304 269.7 304H178.3z"/></svg>
                    Sign in
        </Button>
                    <Dialog open={open} onClose={handleClose}>
                        <>
            <div className="signup-form-container">
                <form className="signup-form" style={{gap: '30px'}}onSubmit={handleSubmit}>
                    <div className="signupLogo">Sign In</div>
                        <TextField
                            helperText = {error === 'User not found' ? 'User not found': error === 'All sign in fields must be filled' && email === '' ? 'All sign in fields must be filled' : ''}
                            error = {error === 'User not found' ? true : error === 'All sign in fields must be filled' && email === '' ? true:false}
                            type="email"
                            onChange={(e) => setEmail(e.target.value)}
                            value={email}
                            label="Email"
                            variant="standard"
                            style={{width: '100%'}}
                            sx={{
                                '& .MuiInputBase-input' : {
                                    color: 'black'
                                  }
                            }}
                        />
                        <TextField
                            helperText = {error === 'Incorrect password' ? 'Incorrect password': error === 'All sign in fields must be filled' && password === '' ? 'All sign in fields must be filled' : ''}
                            error={error === 'Incorrect password' ? true : error === 'All sign in fields must be filled' &&  password == '' ? true : false}
                            type={showPassword ? 'text' : 'password'}
                            variant="standard"
                            label='Password'
                            value={password}
                            onChange={(e) => setPassword(e.target.value)}
                            style={{width: '100%'}}
                            InputProps={{
                                endAdornment: (
                                    <InputAdornment>
                                        <IconButton onClick={handleClickShowPassword}>
                                            {showPassword ? <VisibilityOff style={{color: 'black'}}/> : <Visibility style={{color: 'black'}}/>}
                                        </IconButton>
                                    </InputAdornment>
                                )
                            }}
                            sx={{
                                '& .MuiInputBase-input' : {
                                    color: 'black'
                                  }
                            }}
                        />
                        <button className="signupSubmit" style={{marginTop: "auto"}}>Sign In</button>
                        <div className="another-auth" style={{display: 'flex', gap: '20px', flexDirection: 'column', width: '100%'}}>
                            <button style={{border: '1px solid black', width: '100%', display: 'flex', alignItems: 'center', justifyContent: 'center', height: '40px', gap: '5px', backgroundColor: 'white', borderRadius: '5px'}} >
                                <GitHubIcon className='icon' style={{color: 'black'}}/>
                                <div style={{color: 'black', fontSize: '15px'}}>Continue with GitHub</div>
                            </button>
                            <button style={{border: '1px solid black', width: '100%', display: 'flex', alignItems: 'center', justifyContent: 'center', height: '40px', gap: '5px', backgroundColor: 'white', borderRadius: '5px'}}>
                                <GoogleIcon className='icon'/>
                                <div style={{color: 'black', fontSize: '15px'}}>Continue with Google</div>
                            </button>
                        </div>
                </form>
                <div className="signin">Don't have an account?
                <Link style={{marginLeft: '5px'}} onClick={() => {handleDialogSignUpClick(); handleClose()}}>Sign Up!
                </Link>
                </div>
            </div>
                        </>
                    </Dialog>
                                <Dialog open={open2} onClose={() => {handleDialogSignUpClose(); (handleClose())}}>
                                <div className="signup-form-container">
                            <form style={{gap: '30px'}} className="signup-form" onSubmit={handleSubmitSignup}>
                                <div className="signupLogo">Sign Up</div>
                                    <TextField
                                        helperText = {errorSignup === 'Username already in use' ? 'Username already in use' : errorSignup === 'All signup fields must be filled' && username == '' ? 'All signup fields must be filled' : false}
                                        error = {errorSignup === 'Username already in use' ? true: errorSignup === 'All signup fields must be filled' && username == '' ? true: false}
                                        variant="standard"
                                        label='Username'
                                        onChange={(e) => setUsername(e.target.value)}
                                        value={username}
                                        style={{width: '100%'}}
                                        sx={{
                                            '& .MuiInputBase-input' : {
                                                color: 'black'
                                              }
                                        }}
                                    />
                                    <TextField
                                        helperText = {errorSignup === 'User with this email already exists' ? 'User with this email already exists' : errorSignup === 'Enter a valid email' ? 'Enter a valid email' : errorSignup === 'All signup fields must be filled' && email === '' ? 'All signup fields must be filled': ''}
                                        error = {errorSignup === 'User with this email already exists' ? true : errorSignup === 'Enter a valid email' ? true : errorSignup === 'All signup fields must be filled' && email == '' ? true: false} 
                                        variant="standard"
                                        label='Email'
                                        value={email}
                                        onChange={(e) => setEmail(e.target.value)}
                                        style={{width: '100%'}}
                                        sx={{
                                            '& .MuiInputBase-input' : {
                                                color: 'black'
                                              }
                                        }}
                                    />
                                    <TextField
                                        helperText = {errorSignup === 'Enter a stronger password' ? 'Enter a stronger password' : errorSignup === 'All signup fields must be filled' && password === '' ? 'All signup fields must be filled' : ''}
                                        error={errorSignup === 'Enter a stronger password' ? true : errorSignup === 'All signup fields must be filled' && password == '' ? true : false}
                                        type={showPassword ? 'text' : 'password'}
                                        variant="standard"
                                        value={password}
                                        onChange={(e) => setPassword(e.target.value)}
                                        label="Password"
                                        style={{width: '100%'}}
                                        InputProps={{
                                            endAdornment: (
                                                <InputAdornment>
                                                    <IconButton onClick={handleClickShowPassword}>
                                                        {showPassword ? <VisibilityOff style={{color: 'black'}}/> : <Visibility style={{color: 'black'}}/>}
                                                    </IconButton>
                                                </InputAdornment>
                                            )
                                        }}
                                        sx={{
                                            '& .MuiInputBase-input' : {
                                                color: 'black'
                                              }
                                        }}
                                    />
                                    <button className="signupSubmit" style={{marginTop: 'auto'}}>Sign Up</button>
                                    <div className="another-auth" style={{display: 'flex', gap: '20px', flexDirection: 'column', width: '100%'}}>
                                        <button style={{border: '1px solid black', width: '100%', display: 'flex', alignItems: 'center', justifyContent: 'center', height: '40px', gap: '5px', backgroundColor: 'white', borderRadius: '5px'}} >
                                            <GitHubIcon className='icon' style={{color: 'black'}}/>
                                            <div style={{color: 'black', fontSize: '15px'}}>Continue with GitHub</div>
                                        </button>
                                        <button style={{border: '1px solid black', width: '100%', display: 'flex', alignItems: 'center', justifyContent: 'center', height: '40px', gap: '5px', backgroundColor: 'white', borderRadius: '5px'}}>
                                            <GoogleIcon className='icon'/>
                                            <div style={{color: 'black', fontSize: '15px'}}>Continue with Google</div>
                                        </button>
                                    </div>
                            </form>
                        </div>
                    </Dialog>
                </>
     );
}
 
export default Signin;