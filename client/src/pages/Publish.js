import { useState } from "react";
import {usePublish} from '../hooks/usePublish'
import {useAuthContext} from '../context/AuthContext'
import ImageUploading from 'react-images-uploading'
import TextField from '@mui/material/TextField'
import Select from 'react-select'
import { Button, FormControl, MenuItem} from "@mui/material";
import {Select as MuiSelect} from '@mui/material'
import {InputLabel} from '@mui/material'
import { Navigate } from "react-router-dom";

const Publish = () => {
    const [problemTitle, setProblemTitle] = useState('')
    const [problemText, setProblemText] = useState('')
    const [problemType, setProblemType] = useState()
    const [problemTags, setProblemTags] = useState([])
    const [problemDifficulty, setProblemDifficulty] = useState([])
    const [problemDataIn, setProblemDataIn] = useState('');
    const [problemDataOut, setProblemDataOut] = useState('');
    const [problemRestriction, setProblemRestriction] = useState('');
    const [problemInputFile, setProblemInputFile] = useState('null');
    const [problemOutputFile, setProblemOutputFile] = useState('null');
    const [problemTimeExecution, setProblemTimeExecution] = useState('')
    const [problemClasa, setProblemClasa] = useState('')
    /* const [problemImages, setProblemImages] = useState([]) */
    const [problemLimitMemory, setProblemLimitMemory] = useState('')
    const [numarExemple, setNumarExemple] = useState(1);
    const [numarTeste, setNumarTeste] = useState(1)
    const [examples, setExamples] = useState([
        { exemplu: 'exemplu1', input: '', output: '', explicatie: '' },
        { exemplu: 'exemplu2', input: '', output: '', explicatie: ''  },
        { exemplu: 'exemplu3', input: '', output: '', explicatie: ''  },
        { exemplu: 'exemplu4', input: '', output: '', explicatie: ''  },
        { exemplu: 'exemplu5', input: '', output: '', explicatie: ''  },
        { exemplu: 'exemplu6', input: '', output: '', explicatie: ''  },
        { exemplu: 'exemplu7', input: '', output: '', explicatie: ''  },
        { exemplu: 'exemplu8', input: '', output: '', explicatie: ''  },
        { exemplu: 'exemplu9', input: '', output: '', explicatie: ''  },
        { exemplu: 'exemplu10', input: '', output: '', explicatie: ''  },
    ]);
    const [teste, setTeste] = useState([
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},
        {input: '', output: '', points: ''},

    ])
    const testeFiltrate = teste.filter((test) => test.input!=='') 
    const tagOptions = [
        { value: 'OJI', label: 'OJI' },
        { value: 'ONI', label: 'ONI' },
        { value: 'Lotul National', label: 'Lotul National' },
        { value: 'Olimpiada balcanica de informatica pentru juniori', label: 'Olimpiada balcanica de informatica' },
        { value: 'Info-Oltenia', label: 'Info-Oltenia' },
        { value: 'UNIBUC', label: 'UNIBUC' },
        { value: 'UBB', label: 'UBB' },
        { value: 'UAIC', label: 'UAIC' },
        { value: 'UPB', label: 'UPB' },
        { value: 'UVT', label: 'UVT' },
        { value: 'UTGA', label: 'UTGA' },
        { value: 'Structuri repetitive', label: 'Structuri repetitive' },
        { value: 'Prelucrarea numerelor', label: 'Prelucrarea numerelor' },
        { value: 'Prelucrarea unor secvente de valori', label: 'Prelucrarea unor secvente de valori' },
        { value: 'Structuri decizionale', label: 'Structuri decizionale' }
    ];
    const difficultyOptions = [
        {value: 'usoara', label: 'Usoara'},
        {value: 'medie', label: 'Medie'},
        {value: 'dificila', label: 'Dificila'},
        {value: 'concurs', label: 'Concurs'},
        {value: 'interviu', label: 'Interviu'},
    ]
    const claseOptions = [
        { value: 'Clasa a IX-a', label: 'Clasa a IX-a' },
        { value: 'Clasa a X-a', label: 'Clasa a X-a' },
        { value: 'Clasa a XI-a', label: 'Clasa a XI-a' },
    ]
    
    ////////////////////////////////////////////////////////////////////////////
    const {user} = useAuthContext()
    const exempleFiltrate = examples.filter((example) => example.input !== '')
    const {publish, error, loading} = usePublish()
    const handlePublish = (e) => {
        e.preventDefault()
        publish(
             user.username,
             problemTitle,
             problemText,
             problemType,
             problemTags,
             problemDifficulty,
             problemDataIn,
             problemDataOut,
             problemInputFile,
             problemOutputFile,
             problemRestriction,
             problemTimeExecution,
             exempleFiltrate,
             testeFiltrate,
             problemLimitMemory,
             problemClasa
        )
        if(!error){
            <Navigate to={`/${problemTitle}`}></Navigate>
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    const handleChangeOptions = (options) => {
        const selectedOptions = options.map((option) => option.value)
        setProblemTags(selectedOptions);
    }
    ////////////////////////////////////////////////////////////////////////////
    const customStylesMulti = {
        option: (provided, state) => ({
          ...provided,
          backgroundColor: state.isSelected ? '#007bff' : state.isFocused ? '#313131' : null,
          color: state.isFocused ? '#fff' : '#333',
          cursor: 'pointer',
        }),
        menu: (provided) => ({
            ...provided,
            zIndex: '2',
        })
    };
    ////////////////////////////////////////////////////////////////////////////
    const handleExampleInputChange = (index) => (e) => {
        const newExampleInput = e.target.value;
        setExamples((prevExamples) => {
            const updatedExamples = [...prevExamples];
            updatedExamples[index].input = newExampleInput;
            return updatedExamples;
        });
    };
    const handleExampleOutputChange = (index) => (e) => {
        const newExampleOutput = e.target.value
        setExamples((prevExamples) => {
            const updatedExamples = [...prevExamples]
            updatedExamples[index].output = newExampleOutput;
            return updatedExamples
        })
    }
    const handleExampleExplicationChange = (index) => (e) =>{
        const newExampleExplication = e.target.value
        setExamples((prevExamples) => {
            const updatedExamples = [...prevExamples]
            updatedExamples[index].explicatie = newExampleExplication
            return updatedExamples
        })
    }
    const handleTestInputChange = (index) => (e) => {
        const newInput = e.target.value 
        setTeste((prevTeste) => {
            const updatedTests = [...prevTeste]
            updatedTests[index].input = newInput
            return updatedTests
        })
    }
    const handleTestOutputChange = (index) => (e) => {
        const newOutput= e.target.value
        setTeste((prevTeste) => {
            const updatedTests = [...prevTeste];
            updatedTests[index].output = newOutput
            return updatedTests
        })
    }
    const handleTestPointsChange = (index) => (e) =>{
        const newPoints = e.target.value 
        setTeste((prevTeste) => {
            const updatedTests = [...prevTeste]
            updatedTests[index].points = newPoints
            return updatedTests
        })
    }
   /*  const handlePictureChange = (imageList) => {
        setProblemImages(imageList);
    } */
    let examplesInputs = []
    const addNewExample = (numberOfExamples) => {
        examplesInputs = []
        for(let i = 0; i < numberOfExamples; ++i){
            examplesInputs.push(
                    <div style={{gap: '15px', display: 'flex', marginTop: '40px', flexDirection: 'column', marginRight: '20px', marginLeft: '20px'}}>
                            <h3 style={{marginLeft: '5px', marginRight: '20px', color: 'white'}}>{`Exemplul ${i+1}`}</h3>
                        <TextField
                            style={{backgroundColor: '#313131'}}
                            key={`example ${i}`} 
                            label='Input exemplu'
                            sx={{
                                '& .MuiOutlinedInput-root': {
                                    color: 'white',
                                },
                            }}
                            value={examples[i].input}
                            multiline
                            onChange={handleExampleInputChange(i)}
                            required
                            variant="filled"
                        />
                        <TextField 
                            style={{backgroundColor: '#313131'}}
                            label='Output exemplu'
                            multiline
                            sx={{
                                '& .MuiInputBase-input':{
                                    color: 'white'
                                }
                            }}
                            onChange={handleExampleOutputChange(i)}
                            value={examples[i].output}
                            required
                            variant="filled"
                        />
                        <TextField
                            style={{marginBottom: '15px', backgroundColor: '#313131'}} 
                            label='Explicatie (optional)'
                            sx={{
                                '& .MuiInputBase-input':{
                                    color: 'white'
                                }
                            }}
                            multiline
                            onChange={handleExampleExplicationChange(i)}
                            value={examples[i].explicatie}
                            variant="filled"
                        />
                    </div>
            )
        }
    }
    let testInputs = []
    const testsInputs = (numarTeste) => {
        testInputs = []
        for(let i = 0; i < numarTeste; ++i){
            testInputs.push(
            <div className="add-test" style={{gap: '15px', display: 'flex', marginTop: '40px', flexDirection: 'column', marginRight: '20px', marginLeft: '20px', marginTop: '-10px'}}>
                    <h3 style={{marginLeft: '5px', marginRight: '20px', color: 'white'}}>{`Testul ${i+1}`}</h3>
                        <TextField 
                            label='Input'
                            sx={{
                                '& .MuiInputBase-input':{
                                    color: 'white'
                                }
                            }}
                            multiline
                            onChange={handleTestInputChange(i)}
                            required
                            variant="filled"
                            style={{backgroundColor: '#313131', overflow: 'auto'}}
                            maxRows={5}
                        />
                        <TextField 
                            label='Output'
                            sx={{
                                '& .MuiInputBase-input':{
                                    color: 'white'
                                }
                            }}
                            multiline
                            onChange={handleTestOutputChange(i)}
                            required
                            variant="filled"
                            style={{backgroundColor: '#313131', overflow: 'auto'}}
                            maxRows={5}
                        />
                        <TextField 
                            style={{marginBottom: '15px', backgroundColor: '#313131'}}
                            label='Puncte'
                            required
                            sx={{
                                '& .MuiInputBase-input':{
                                    color: 'white'
                                }
                            }}
                            onChange={handleTestPointsChange(i)}
                            variant="filled"
                        />
                </div>
            )
        }
    }
    return ( 
        <div className="publish">
            <div className="enunt_si_cerinta">
                <h1 style={{marginLeft: '20px', marginTop: '20px', color: 'white', marginBottom: '10px'}}>Enunt & cerinta</h1>
                    <TextField
                        className="marginInput"                        
                        style={{marginLeft: '20px', marginRight: '20px' ,backgroundColor: '#313131'}}
                        variant="filled"  
                        label="Titlul problemei"
                        onChange={(e) => setProblemTitle(e.target.value)}
                        InputProps={{
                            style: {color: 'white'}
                        }}
                    />
                    <TextField
                        style={{marginLeft: '20px', marginRight: '20px', overflow: 'auto', borderRadius: '7px', backgroundColor: '#313131', marginBottom: '20px'}}
                        className="marginInput"
                        variant="filled"
                        label="Enuntul problemei"
                        onChange={(e) => setProblemText(e.target.value)}
                        InputProps={{
                            style: {color: 'white'}
                        }}
                        multiline
                        maxRows={10}
                        minRows={10}
                    />
            </div>
            <div className="info-problem">
                <h1 style={{marginLeft: '20px', marginTop: '20px', color: 'white', marginBottom: '10px'}}>Info</h1>
                    <FormControl sx={{marginLeft: '20px', marginRight: '20px'}}>
                        <InputLabel>Tipul problemei</InputLabel>
                                <MuiSelect
                                    onChange={(e) => setProblemType(e.target.value)}
                                    label="Tipul problemei"
                                    value={problemType}
                                    SelectDisplayProps={{ style: { backgroundColorcolor: 'white' } }}
                                    sx={{
                                        '& .MuiSvgIcon-root': {
                                            fill: 'white'
                                        },
                                        '& .MuiInputBase-input': {
                                            color: 'white'
                                        }
                                    }}
                                >
                                    <MenuItem value={'consola'}>Consola</MenuItem>
                                    <MenuItem value={'fisiere'}>Fisiere</MenuItem>
                            </MuiSelect>
                        </FormControl>
                        <FormControl sx={{marginLeft: '20px', marginRight: '20px'}}>
                            <InputLabel>Clasa</InputLabel>
                            <MuiSelect
                                label="Clasa"
                                SelectDisplayProps={{ style: { color: 'white' } }}
                                sx={{
                                    '& .MuiSvgIcon-root': {
                                        fill: 'white'
                                    }
                                }}
                                onChange={(e) => setProblemClasa(e.target.value)}
                            >
                                {claseOptions.map((clasa) => (
                                    <MenuItem value = {clasa.label}>{clasa.label}</MenuItem>
                                ))}
                            </MuiSelect>
                    </FormControl>
                    <Select
                        className="select-tags"
                        options={tagOptions}
                        isMulti
                        onChange={handleChangeOptions}
                        styles={customStylesMulti}
                        placeholder='Tagurile problemei'
                    />
                    <FormControl sx={{marginLeft: '20px', marginRight: '20px'}}>
                            <InputLabel>Dificultatea problemei</InputLabel>
                            <MuiSelect
                                label="Dificultate problemei"
                                SelectDisplayProps={{ style: { color: 'white' } }}
                                sx={{
                                    '& .MuiSvgIcon-root': {
                                        fill: 'white'
                                    }
                                }}
                                onChange={(e) => setProblemDifficulty(e.target.value)}
                            >
                                {difficultyOptions.map((clasa) => (
                                    <MenuItem value = {clasa.label}>{clasa.label}</MenuItem>
                                ))}
                            </MuiSelect>
                    </FormControl>
                    <TextField
                    variant='filled'
                    label="Date de intrare"
                    onChange={(e) => setProblemDataIn(e.target.value)}
                    style={{marginRight: '20px', marginLeft: '20px', backgroundColor: '#313131'}}
                    sx={{
                        '& .MuiOutlinedInput-root': {
                            color: 'white'
                        }
                    }}
                    multiline
                />
                {problemType === 'fisiere' && (
                    <>
                        <TextField
                            variant="filled"
                            type="" 
                            label="Fisierul .in/.txt"
                            style={{marginLeft: '20px', marginRight: '20px', backgroundColor: '#313131'}}
                            onChange={(e) => setProblemInputFile(e.target.value)}
                            sx={{
                                '& .MuiOutlinedInput-root': {
                                    color: 'white'
                                }
                            }}
                        />    
                    </>
                    
                )}
                <TextField
                    variant='filled'
                    label="Date de iesire"
                    onChange={(e) => setProblemDataOut(e.target.value)}
                    style={{marginRight: '20px', marginLeft: '20px', backgroundColor: '#313131'}}
                    sx={{
                        '& .MuiOutlinedInput-root': {
                            color: 'white'
                        }
                    }}
                    multiline
                />
                {problemType === 'fisiere' && (
                    <>
                        <TextField 
                            variant="filled"
                            label='Fisierul .out/.txt'
                            style={{marginLeft: '20px', marginRight: '20px', backgroundColor: '#313131'}}
                            onChange={(e) => setProblemOutputFile(e.target.value)}    
                            sx={{
                                '& .MuiOutlinedInput-root': {
                                    color: 'white'
                                }
                            }}
                        />
                    </>
                )}
                <TextField 
                    variant="filled"
                    label="Timp de executie"
                    onChange={(e) => setProblemTimeExecution(e.target.value)}
                    style={{marginLeft: '20px', marginRight: '20px', backgroundColor: '#313131'}}
                    sx={{
                        '& .MuiInputBase-input':{
                            color: 'white'
                        }
                    }}
                />
                <TextField
                    style={{marginLeft: '20px', marginRight: '20px', backgroundColor: '#313131'}}
                    variant="filled"
                    label='Limita de memorie'
                    onChange={(e) => setProblemLimitMemory(e.target.value)}
                    sx={{
                        '& .MuiInputBase-input':{
                            color: 'white'
                        }
                    }}
                />
                <TextField
                    variant="filled"
                    label='Restrictii si precizari'
                    style={{marginLeft: '20px', marginRight: '20px', backgroundColor: '#313131', marginBottom: '10px'}}
                    onChange={(e) => setProblemRestriction(e.target.value)}
                    multiline
                    minRows={5}
                    maxRows={5}
                />
            </div>
            <div className="example-button">
                <h1 style={{marginLeft: '20px', color: 'white', marginTop: '5px', marginBottom: '10px'}}>Exemple</h1>
                    <Button 
                        variant="outlined"
                        style={{color: 'white', border: '1px solid white', marginLeft: '20px'}}
                        onClick={() => {
                            setNumarExemple(numarExemple+1)
                        }}
                    >
                        Adauga exemplu
                    </Button>
                    {addNewExample(numarExemple)}
                    {examplesInputs}
            </div>
            <div className="teste">
                <h1 style={{marginLeft: '20px', marginTop: '20px', color: 'white'}}>Teste</h1>
                <div className="test-button">
                    <Button
                        variant="outlined"
                        style={{color: 'white', border: '1px solid white'}}
                        onClick={() => {
                            setNumarTeste(numarTeste + 1)
                        }}
                    >
                        Adauga test
                    </Button>
                </div>
                {testsInputs(numarTeste)}
                {testInputs}
            </div>
{/*             <ImageUploading
                multiple
                value={problemImages}
                onChange={handlePictureChange}
                dataURLKey="data_url"
            >
            {({imageList, onImageRemove, onImageRemoveAll, dragProps, onImageUpload}) => (
                <div className="pictures-wrapper">
                    <button onClick={onImageUpload} onChange={() => handlePictureChange(imageList)}{...dragProps}>Upload image or drag image</button>
                    <button onClick={onImageRemoveAll}>Remove all images</button>
                    <div className="image"> 
                        {imageList.map((image, index) => (
                            <div>
                                <img src={image['data_url']} key={index} width='170'></img>
                                <div className="remove-button">
                                    <button onClick={onImageRemove}>Remove Image</button>
                                </div>
                            </div>
                        ))}
                    </div>
                </div>
            )}
      </ImageUploading> */}
            <button onClick={handlePublish} className="publish-button">Publish</button>
            {error && <div className={`${error}` ? 'error margin-top' : 'not-error'} style={{position: 'fixed', zIndex: '2'}}>
            <div className="white-space"></div>
                <div className="error-icon"></div>
                    <div className="error-class">
                        <svg xmlns="http://www.w3.org/2000/svg" height="1em" viewBox="0 0 512 512"><path d="M256 48a208 208 0 1 1 0 416 208 208 0 1 1 0-416zm0 464A256 256 0 1 0 256 0a256 256 0 1 0 0 512zM175 175c-9.4 9.4-9.4 24.6 0 33.9l47 47-47 47c-9.4 9.4-9.4 24.6 0 33.9s24.6 9.4 33.9 0l47-47 47 47c9.4 9.4 24.6 9.4 33.9 0s9.4-24.6 0-33.9l-47-47 47-47c9.4-9.4 9.4-24.6 0-33.9s-24.6-9.4-33.9 0l-47 47-47-47c-9.4-9.4-24.6-9.4-33.9 0z"/></svg>
                            <div className="error-text">
                                {error} 
                            </div>  
                    </div>
                </div>} 
        </div>
        
     );
}


export default Publish;