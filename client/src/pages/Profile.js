const Profile = ({user}) => {
    return ( 
        <div className="profile-wrap">
            <div className="profile-info">
                <div className="profile-picture" style={{backgroundColorcolor: 'black'}}></div>
                    <div className="profile-username">
                        {user.username}
                    </div>
            </div>
        </div>
     );
}
 
export default Profile;