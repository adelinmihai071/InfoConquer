import { useState } from "react"
import { useAuthContext } from "../context/AuthContext";
export const useSignup = () => {
    const [errorSignup, setError] = useState(null);
    const [loading, setLoading] = useState(null);
    const {dispatch} = useAuthContext();
    const signup = async(email, password, username) => {

        setLoading(true);
        setError(null)

        const response =  await fetch('https://ic2-backend.onrender.com/user/signup', {
            method: "POST",
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify({email, password, username})
        })

        const userData = await response.json();
        if(!response.ok){
            setError(userData.error);
            setLoading(false);
        }else{
            setError(null);
            setLoading(false);
            localStorage.setItem('user', JSON.stringify(userData));
            dispatch({type: "SIGNUP", payload: userData});
        }
    }
    return {signup, errorSignup, loading};
}