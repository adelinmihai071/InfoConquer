import { useEffect, useState } from "react";

export const useGetSolutions = () => {
  const [solutions, setSolutions] = useState(null);
  const fetchData = async () => {
    try {
      const response = await fetch('https://ic2-backend.onrender.com/problem/getSolutii', {
        method: "GET",
        headers: {
          "Content-Type": "application/json"
        }
      });

      if (response.ok) {
        const solutii = await response.json();
        setSolutions(solutii.solutions);
      } else {
        console.log("Failed to fetch solutions");
      }
    } catch (error) {
      console.error("An error occurred while fetching solutions:", error);
    }
  };
  useEffect(() => {
    fetchData(); 
  }, []);
  return solutions ;
};
