import { useState } from "react"

export const usePublish = () => {
    const [error, setError] = useState(null)
    const [loading, setLoading] = useState(true)
    const publish = async(creator_username,
         titlu, 
         cerinta, 
         tip, 
         tags, 
         dificultate, 
         input,
         output, 
         inputFile, 
         outputFile, 
         restrictii, 
         timp,
         exemple,
         teste,
         limitaMemorie,
         clasa) => {
        const response = await fetch('https://ic2-backend.onrender.com/problem/add', {
            method: 'POST',
            headers: {'Content-Type': "application/json"},
            body: JSON.stringify({creator_username, titlu, cerinta, tip, tags, dificultate, input, output, inputFile, outputFile, restrictii, timp, exemple, teste, limitaMemorie, clasa})
        })
        const  problemData = await response.json()
        if(!problemData.ok){
            setError(problemData.error)
            setLoading(false)
        }else{
            setLoading(false)
            setError(null)
        }
    }
    return {publish, error, loading}
}