import { useState } from "react";
import { useAuthContext } from "../context/AuthContext"
export const useSignin = () => {
    const {dispatch} = useAuthContext();
    const [error, setError] = useState(null);
    const [loading, setIsLoading] = useState(false);
    const signin = async(email, password) => {
        setError(null);
        setIsLoading(true);
        const response = await fetch('https://ic2-backend.onrender.com/user/signin', {
            method: 'POST',
            headers : {'Content-Type': 'application/json'},
            body: JSON.stringify({email, password})
        })

        const userData = await response.json();

        if(!response.ok){
            setIsLoading(false);
            setError(userData.error)
        }else{
            setIsLoading(false);
            setError(null);
            localStorage.setItem('user', JSON.stringify(userData));
            dispatch({type: 'LOGIN', payload: userData})
        }
    }
    return {signin, error, loading};
}