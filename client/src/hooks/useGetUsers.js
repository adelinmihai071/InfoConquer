import { useEffect, useState } from "react"
export const useGetUser = () => {
        const [users, setUsers] = useState(null)
        const fetchUsers = async () => {
            const response = await fetch('https://ic2-backend.onrender.com/getUsers', {
                method : 'GET',
                headers: {'Content-Type': "application/json"},
            })
            const data = await response.json()
            data ? setUsers(data) : setUsers(null)
        }
        useEffect(() => {
            fetchUsers();
        }, [])
        return users
}