import { useEffect, useState } from "react";
export const useGetProblems = () => {
  const [error, setError] = useState(null);
  const [problems, setProblems] = useState({problems: []});

  useEffect(() => {
    const getProblems = async () => {
      try {
        const response = await fetch("https://ic2-backend.onrender.com/getProblems", {
          method: "GET",
          headers: { "Content-Type": "application/json" },
        });
  
        if (!response.ok) {
          throw new Error("Error fetching data");
        }
  
        const data = await response.json();
        setProblems(data);
      } catch (error) {
        setError(error.message);
      }
    };
    getProblems();
  }, []);
  return {error, problems};
};