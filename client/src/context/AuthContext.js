import { createContext, useContext, useEffect, useReducer } from "react";

export const AuthContext = createContext();
export const stateReducer = (state, action ) => {
    if(action.type === 'LOGIN' || action.type === 'SIGNUP'){
        return {user: action.payload}
    }else if(action.type === 'LOGOUT'){
        return {user: null}
    }else{
        return null;
    }
}
const initialUserState = {
    user: null
}
export const useAuthContext = () => {
    const userContext = useContext(AuthContext);
    if(userContext){
        return userContext;
    }else{
        return 'No current user';
    }
}
export const AuthContextProvider = ({children}) => {
    const [state, dispatch] = useReducer(stateReducer, initialUserState);
    useEffect(() => {
        const userData = JSON.parse(localStorage.getItem('user'));
        if(userData){
            dispatch({type: 'LOGIN', payload: userData});
        }
    }, [])
    return ( 
        <AuthContext.Provider value={{...state, dispatch}}>
            {children}
        </AuthContext.Provider>
     );
}