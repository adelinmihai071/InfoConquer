import { BrowserRouter, Routes, Route } from "react-router-dom";
import { Navigate } from "react-router-dom";
import Home from "./pages/Home";
import Navbar from "./components/Navbar";
import { useAuthContext } from "./context/AuthContext";
import Publish from "./pages/Publish";
import Problems from "./pages/Problems";
import Problem from "./components/Problem";
import { useGetProblems } from "./hooks/useGetProblems";
import ProblemElement from "./components/ProblemElement";
import { useState } from "react";
import Profile from "./pages/Profile";
import { useGetUser } from "./hooks/useGetUsers";
import Solutie from "./components/Solutie";
import { useGetSolutions } from "./hooks/useGetSolutions";
import Solutions from './components/Solutions'
function App (){
  const {user} = useAuthContext();
  const {error, problems} = useGetProblems()
  const users = useGetUser();
  const solutions = useGetSolutions();
  const routeSolution = () => {
    return (
      <Route 
        path="/solutions"
        element={<Solutions solutii={solutions}/>}
    />
    )
  }
  const genRoutes = () => {
    return problems.problems.map((problem) => (
      <>
      <Route 
        path={`/${problem.titlu}`}
        element={<Problem problem={problem} />}
      />
      <Route 
          path={`/solutii/${problem.titlu}`}
          element={<Solutie problema={problem.titlu}></Solutie>}
        />
      </>
    ));
  };
  const [problemsGet, setProblems] = useState();
  const problemsCriteria = ['OJI', 'ONI', 'Olimpiada balcanica de informatica pentru juniori', 'Info-Oltenia', 'UNIBUC', 'UBB', 'UAIC', 'UPB', 'UVT', 'UTGA', 'Structuri repetitive', 'Prelucrarea numerelor', 'Prelucrarea unor secvente de valori', 'Structuri decizionale'];
  const genProblems = () => {
    return problemsCriteria.map((problem) => (
      <>
        <Route
            path={`/${problem}`}
            element={<ProblemElement problem={problems.problems.filter((prob) => prob.tags.includes(problem))}/>}
        />
      </>
    ))
  }
  const genProfileRoutes = (users) => {
    return users.user.map((user) => (
      <Route 
        path={`/${user.username}`}
        element={<Profile user={user} />}
      />
    ))
  } 
  return (  
    <div>
      <BrowserRouter>
        <Navbar/>
        <Routes>
        <Route
          path = '/'
          element={<Home/>}
        />
        <Route 
          path="/problems"
          element={<Problems/>}
        />
        <Route
          path="/add"
          element={user ? <Publish/> : <Navigate to='/'/>}
        />
        {genRoutes()}
        {genProblems()}
        {routeSolution()}
        {users && genProfileRoutes(users)}
        </Routes>
      </BrowserRouter>
    </div>
  );
}
  
export default App;