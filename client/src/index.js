import { AuthContextProvider } from './context/AuthContext';
import React from 'react';
import ReactDOM from 'react-dom/client';
import './styles/index.css';
import './styles/home.css';
import './styles/problems.css'
import './styles/publish.css'
import './styles/navbar.css'
import './styles/signin.css'
import './styles/problem.css'
import './styles/profile.css'
import "bootstrap/dist/css/bootstrap.css"
import App from './App';


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <AuthContextProvider>
          <App />
      </AuthContextProvider>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
