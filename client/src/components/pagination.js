import { Range } from '../components/range';

const Pagination = (props) => {
  const pageChangeHandler = (value) => {
    props.onPageChange(value);
  };

  const pageItems = Range(props.totalPage, props.page, props.siblings);

  return (
    <ul className="pagination pagination-md justify-content-end">
      <li className="page-item">
        <span className="page-link" onClick={() => pageChangeHandler('&laquo;')}>&laquo;</span>
      </li>
      <li className="page-item">
        <span className="page-link" onClick={() => pageChangeHandler('&lsaquo;')}>&lsaquo;</span>
      </li>
      {pageItems.map((value) => (
        <li key={value} className={`page-item ${value === props.page ? 'active' : ''}`}>
          <span className="page-link" onClick={() => pageChangeHandler(value)}>{value}</span>
        </li>
      ))}
      <li className="page-item">
        <span className="page-link" onClick={() => pageChangeHandler('&rsaquo;')}>&rsaquo;</span>
      </li>
      <li className="page-item">
        <span className="page-link" onClick={() => pageChangeHandler('&raquo;')}>&raquo;</span>
      </li>
    </ul>
  );
};

export default Pagination;
