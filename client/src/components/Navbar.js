import { useState } from 'react';
import {Link} from 'react-router-dom'
import { useAuthContext } from '../context/AuthContext';
import {useLogout} from '../hooks/useLogout'
import Signin from '../pages/Signin';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import DarkModeIcon from '@mui/icons-material/DarkMode';
import {Switch} from '@mui/material'
import SettingsIcon from '@mui/icons-material/Settings';
import CancelIcon from '@mui/icons-material/Cancel';
const Navbar = () => {
    const {user} = useAuthContext();
    const [searchQuery, setSearchQuery] = useState('');
    const [menuClicked, setMenuClicked] = useState(false);
    const [profileClicked, setProfileClicked] = useState(false)
    const logout = useLogout();
    return (
            <header className="navbara" style={{zIndex: '4'}}>
            <Link to='/'className='logo'>
                    &lt; InfoConquer/ &gt;
            </Link>
            <nav className='menu-navbar'>
                <div className="routes">
                    <div className="problemsRoute">
                        <Link to='/problems'>Problems</Link>
                    </div>
                    <div className="solutionsRoute">
                        <Link to='/solutions'>Solutions</Link>
                    </div>
                </div>
            <div className="search">
                <span>
                    <svg xmlns="http://www.w3.org/2000/svg" height="1em" viewBox="0 0 512 512"><path d="M416 208c0 45.9-14.9 88.3-40 122.7L502.6 457.4c12.5 12.5 12.5 32.8 0 45.3s-32.8 12.5-45.3 0L330.7 376c-34.4 25.2-76.8 40-122.7 40C93.1 416 0 322.9 0 208S93.1 0 208 0S416 93.1 416 208zM208 352a144 144 0 1 0 0-288 144 144 0 1 0 0 288z"/></svg>
                </span>
                <input
                    onChange={(e) => setSearchQuery(e.target.value)}
                    type="text" 
                    placeholder='Search...'
                />
            </div>
            {user && 
            <div className="name" class="d-flex flex-column align-items-end" >
                <div className="profile-picture" onClick={() => setProfileClicked((prev) => !prev)}></div>
            </div>}
                {!user &&                 
                    <Signin />
                }
             <div className={`menu-btn ${menuClicked ? 'open': ''}`} onClick={() => setMenuClicked((prev) => !prev)}>
                    <div></div>
            </div>
            </nav>
            <div className="profile-info" style={{width: '300px', height: '200px', position: 'absolute', right: '79px', top: '50px', display: `${profileClicked ? 'block' : 'none'}`}}>
                <div style={{margin: '20px', fontSize: 'x-large', marginBottom: '5px', display: 'flex', alignItems: 'center', justifyContent: 'space-between'}}>
                    {user && user.username}
                    <CancelIcon onClick={() => setProfileClicked(false)} className='cancelIcon'/>
                </div>
                <div className="break-line" style={{height: '1px', width: 'calc(100% - 40px)', backgroundColor: 'grey', marginLeft: '20px'}}></div>
                <div className="options" style={{marginTop: '10px', display: 'flex', flexDirection: 'column', gap: '10px'}}>
                    <Link style={{color: 'black'}} to={`/${user && user.username}`} onClick={() => setProfileClicked(false)}>
                        <div style={{marginLeft: '20px', display: 'flex', gap: '10px', fontSize: '20px', alignItems: 'center'}}>
                            <AccountCircleIcon/>
                            See profile
                        </div>
                    </Link>
                    <div style={{marginLeft: '20px',marginRight: '20px', display: 'flex', fontSize: '20px', alignItems: 'center', justifyContent: 'space-between'}}>
                        <div style={{display: 'flex', alignItems: 'center', gap: '10px'}}>
                            <DarkModeIcon/>
                            Dark mode
                        </div>
                        <Switch defaultChecked/>
                    </div>
                    <div style={{marginLeft: '20px', display: 'flex', gap: '10px', fontSize: '20px', alignItems: 'center'}}>
                        <SettingsIcon/>
                        Settings
                    </div>
                </div>
            </div>
    <div className={`dropdown-menua ${menuClicked ? '' : 'slide-out'}`}>
      <Link to={'/'} onClick={() => setMenuClicked(false)}>
        <div className={`menu-home ${menuClicked ? 'fadeIn' : 'fadeOut'}`}>
          Home
        </div>
      </Link>
      <Link to={'/help'} onClick={() => setMenuClicked(false)}>
        <div className={`menu-help ${menuClicked ? 'fadeIn' : 'fadeOut'}`}>
          Help
        </div>
      </Link>
      <Link to={'/problems'} onClick = {() => setMenuClicked(false)}>
        <div className={`menu-problems ${menuClicked ? 'fadeIn' : 'fadeOut'}`}>
          Problems
        </div>
      </Link>
      <Link to={'/solutions'} onClick={() => setMenuClicked(false)}>
        <div className={`menu-solutions ${menuClicked ? 'fadeIn' : 'fadeOut'}`}>
          Solutions
        </div>
      </Link>
      <Link to={'/settings'} onClick={() => setMenuClicked(false)}>
        <div className={`menu-settings ${menuClicked ? 'fadeIn' : 'fadeOut'}`}>
          Settings
        </div>
      </Link>
    </div>
    </header>
    );
}
 
export default Navbar;
