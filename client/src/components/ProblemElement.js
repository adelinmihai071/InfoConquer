import { Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from '@mui/material';
import {Link} from 'react-router-dom'
import TablePagination from '@mui/material/TablePagination';
import { useState } from 'react';
const ProblemElement = ({problem}) => {
    const columns = [
        {id: 'index', label: 'Index'},
        {id: 'problema', label: 'Problema'},
        {id: 'creator', label: 'Creator'},
        {id: 'clasa', label: 'Clasa'},
        {id: 'tip', label: 'Tip'},
    ]
    function createData(index, problema, creator, clasa, tip){
        return {index, problema, creator, clasa, tip}
    }
    const rows = problem.map((problem, index) => {
            return createData(index, problem.titlu, problem.creator_username, problem.dificultate, problem.tip);
        }) 
    const [page, setPage] = useState(0)
    const [rowsPerPage, setRowsPerPage] = useState(10)
    const handleChangePage = (newPage) => {
        setPage(newPage);
      };
    
    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(+event.target.value);
        setPage(0);
      };
    return ( 
  <div className="elements">
    <Paper sx={{ marginRight: '40px', marginTop: '200px', marginLeft: '40px', width: 'calc(100vw - 80px)'}}>
      <TableContainer>
        <Table>
          <TableHead>
            <TableRow> 
              <TableCell align='center'>Index</TableCell>
              <TableCell align='center'>Problems</TableCell>
              <TableCell align='center'>Creator</TableCell>
              <TableCell align='center'>Dificultate</TableCell>
              <TableCell align='center'>Tip</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((row) => {
                return (
                  <TableRow>
                    {columns.map((column, index) => {
                      const value = row[column.id];
                      return (
                        <TableCell align='center'>
                          {value && index == 1 ? <Link to={`/${value}`} style={{textDecoration: 'none', color: 'black', fontFamily: 'serif'}}>{value.toString()}</Link> : value.toString()}
                        </TableCell>
                      );
                    })}
                  </TableRow>
                );
              })}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[10, 25, 100]}
        count={rows.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
    </Paper>
</div>
     );
}
 
export default ProblemElement;