import _ from "lodash";

export const Range = (totalPage, page, siblings) => {
    const n = 7 + siblings;
    if (n >= totalPage) {
        return _.range(1, totalPage + 1);
    }
    
    const leftIdx = Math.max(page - siblings, 1);
    const rightIdx = Math.min(page + siblings, totalPage);
    
    const leftDots = leftIdx > 2;
    const rightDots = rightIdx < totalPage - 2;
    
    if (!leftDots && rightDots) {
        const leftCount = 3 + 2 * siblings;
        const leftRange = _.range(1, leftCount + 1);
        return [...leftRange, "...", totalPage];
    } else if (leftDots && !rightDots) {
        const rightCount = 3 + 2 * siblings;
        const rightRange = _.range(totalPage - rightCount + 1, totalPage + 1);
        return [1, "...", ...rightRange];
    } else {
        const middleRange = _.range(leftIdx, rightIdx + 1);
        return [1, "...", ...middleRange, "...", totalPage];
    }
}
