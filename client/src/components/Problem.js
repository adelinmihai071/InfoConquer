import React, { useEffect, useRef } from "react";
import { Button, Menu, MenuItem, Paper, Select, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, TextField, Typography } from "@mui/material";
import { Link } from "react-router-dom";
import CodeMirror from '@uiw/react-codemirror' 
import 'codemirror/keymap/sublime';
import { javascript } from '@codemirror/lang-javascript';
import { cpp } from "@codemirror/lang-cpp";
import {java} from '@codemirror/lang-java'
import {python} from "@codemirror/lang-python"
import { dracula } from '@uiw/codemirror-theme-dracula';
import {vscodeDark} from '@uiw/codemirror-theme-vscode'
import {abcdef} from '@uiw/codemirror-theme-abcdef'
import {androidstudio} from '@uiw/codemirror-theme-androidstudio'
import {eclipse} from '@uiw/codemirror-theme-eclipse'
import {atomone} from '@uiw/codemirror-theme-atomone'
import {aura} from '@uiw/codemirror-theme-aura'
import {bbedit} from '@uiw/codemirror-theme-bbedit'
import {bespin} from '@uiw/codemirror-theme-bespin'
import {darcula} from '@uiw/codemirror-theme-darcula'
import {duotoneLight} from '@uiw/codemirror-theme-duotone'
import { duotoneDark } from "@uiw/codemirror-theme-duotone";
import {githubLight} from '@uiw/codemirror-theme-github'
import { githubDark } from "@uiw/codemirror-theme-github";
import {gruvboxDark} from '@uiw/codemirror-theme-gruvbox-dark'
import { gruvboxLight } from "@uiw/codemirror-theme-gruvbox-dark";
import {materialLight} from '@uiw/codemirror-theme-material'
import { materialDark } from "@uiw/codemirror-theme-material";
import {noctisLilac} from '@uiw/codemirror-theme-noctis-lilac'
import {nord} from '@uiw/codemirror-theme-nord'
import {solarizedLight} from '@uiw/codemirror-theme-solarized'
import { solarizedDark } from "@uiw/codemirror-theme-solarized";
import {sublime} from '@uiw/codemirror-theme-sublime'
import {xcodeLight} from '@uiw/codemirror-theme-xcode'
import { xcodeDark } from "@uiw/codemirror-theme-xcode";
import { useState } from "react";
import PlayArrowIcon from '@mui/icons-material/PlayArrow';
import IntegrationInstructionsIcon from '@mui/icons-material/IntegrationInstructions';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import useMediaQuery from '@mui/material/useMediaQuery';
import { useTheme } from '@mui/material/styles';
import { useAuthContext } from "../context/AuthContext";
const Problem = ({ problem }) => {
  const [code, setCode] = useState(
    `#include <iostream>

int main() {
    std::cout << "Hello World";
    return 0;
}
  `
  )
  const languages = [
    { value: 'cpp', label: 'C++'},
    { value: 'cs', label: 'C#'},
    { value: 'c', label: 'C'},
    { value: 'java', label: 'Java'},
    { value: 'javascript', label: 'Javascript'},
    { value: 'python', label: 'Python'},
  ]
  const [theme, setTheme] = useState(dracula)
  const [language, setLanguage] = useState(languages[0].value)
  const themes = [
    { label: 'Dracula', value: dracula },
    { label: 'VSCode Dark', value: vscodeDark },
    { label: 'ABCDEF', value: abcdef },
    { label: 'Android Studio', value: androidstudio },
    { label: 'Eclipse', value: eclipse },
    { label: 'Atom One', value: atomone },
    { label: 'Aura', value: aura },
    { label: 'BBEdit', value: bbedit },
    { label: 'Bespin', value: bespin },
    { label: 'Darcula', value: darcula },
    { label: 'Duotone', value: duotoneLight },
    { label: 'GitHub Light', value: githubLight },
    { label: 'GitHub Dark', value: githubDark },
    { label: 'Duotone Dark', value: duotoneDark},
    { label: 'Gruvbox Dark', value: gruvboxDark },
    { label: 'Gruvbox Light', value: gruvboxLight },
    { label: 'Material Light', value: materialLight },
    { label: 'Material Dark', value: materialDark },
    { label: 'Noctis Lilac', value: noctisLilac },
    { label: 'Nord', value: nord },
    { label: 'Solarized Light', value: solarizedLight },
    { label: 'Solarized Dark', value: solarizedDark },
    { label: 'Sublime', value: sublime },
    { label: 'Xcode Light', value: xcodeLight },
    { label: 'Xcode Dark', value: xcodeDark }
  ];
  const onChangeCode = React.useCallback((value) => {
    setCode(value)
  })
  const [open, setOpen] = useState(false);
  const [testeCompilate, setTesteCompilate] = useState('')
  const Theme = useTheme();
  const fullScreen = useMediaQuery(Theme.breakpoints.down('md'));
  const titlu = problem.titlu
  const [openedInfo, setOpenedInfo] = useState(true);
    const {user} = useAuthContext();
  // TODO: Add a loading state to the component while it's fetching data from the API
  if (!problem || problem.length === 0) {
    return <p>Loading...</p>;
  }
  const teste = problem.teste
  const path = (tag) => `/${tag.split(' ').join('').toLowerCase()}`;
  const handleSubmitCode = async () => {
    setOpen(true)
    const type = 'test'
    const response = await fetch(`https://ic2-backend.onrender.com/${language}`, {
        method: 'POST',
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify({code, type, teste}),
    });
    const responseData = await response.json();
    setTesteCompilate(responseData.testeCompilate)
    const username = user && user.username;
    const solutie = code;
    let punctaj = 0;
    for (const currentPunctaj of testeCompilate) {
      punctaj += parseInt(currentPunctaj.puncte);
    } 
    const titluProblema = problem && problem.titlu;
    const memorie = 1;
    const timp = 1;
    let data = new Date().toString().split(' ');
    data = data[0] + ' ' +  data[1] + ' ' + ' ' +  data[2] + ' ' + ' ' + data[3] + ' ' + data[4];
    const responseSolution = await fetch('https://ic2-backend.onrender.com/problem/postSolution', {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({username, solutie, punctaj, titluProblema, memorie, timp, data, limbaj: language})
    })
    const responseDataSolution = await responseSolution.json();
};
/*   const handleRunCode = async() => {
    const type = 'run'
    const response = await fetch(`http://localhost:4000/${language}`,{
      method:'POST',
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({code, type, terminalInput, titlu})
    })
      const responseData = await response.json();
      setOutput(responseData)
  } */
  const handleClose = () => {
    setOpen(false);
  };
  return (
    <div className="problem-wrapper" style={{marginBottom: '0'}}>
      <div className="problem-info" style={{backgroundColor: '#121212', overflowY: 'auto', borderTopLeftRadius: '5px', borderTopRightRadius: '5px', width: '70%'}}>
        <div className="content-choice" style={{backgroundColor: '#313131', borderTopRightRadius: '1px', borderTopLeftRadius: '1px'}}>
              <Tabs className="content-links" style={{display: 'flex', flex: '1', width: '100%', color: 'white'}}>
                <Tab label="Info"></Tab>
                <Tab label="Solutii"></Tab>
              </Tabs>
        </div>
        <h1 style={{ color: 'white', margin: '20px', backgroundColor: '#313131', textIndent: '15px', borderLeft: '5px solid grey' }}>
          {problem && problem.titlu}
        </h1>
        <TableContainer component={Paper} style={{ width: 'auto', marginTop: '20px', margin: '20px' }}>
          <Table>
            <caption>Infos about the problem</caption>
            <TableHead>
              <TableRow>
                <TableCell align="center">Creator</TableCell>
                <TableCell align="center">Tip</TableCell>
                <TableCell align="center">Dificultate</TableCell>
                <TableCell align="center">Intrare/Iesire</TableCell>
                <TableCell align="center">Limita timp</TableCell>
                <TableCell align="center">Limita memorie</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              <TableCell align="center">{problem.creator_username}</TableCell>
              <TableCell align="center">{problem.tip}</TableCell>
              <TableCell align="center">{problem.dificultate}</TableCell>
              <TableCell align="center">{problem.tip === 'fisiere' ? (`${problem.inputFile}/${problem.outputFile}`) : 'consola'}</TableCell>
              <TableCell align="center">{`${problem.timp}s`}</TableCell>
              <TableCell align="center">{problem.limitaMemorie}</TableCell>
            </TableBody>
          </Table>
        </TableContainer>
        <>
          {problem.tags.map((tag) => (
            <Button
              variant='contained' 
              style={{ marginTop: '20px', marginLeft: '20px', marginBottom: '20px' }} component={Link} to={path(tag)}>
              {tag}
            </Button>
          ))}
        </>
        <h3 style={{ color: 'white', margin: '15px' }}>Cerinta:</h3>
        <p style={{ margin: '15px', color: 'white' }}>
          {problem && problem.cerinta}
        </p>
        <h3 style={{ color: 'white', margin: '15px' }}>Date de intrare:</h3>
        <p style={{ margin: '15px', color: 'white' }}>
          {problem && problem.input}
        </p>
        <h3 style={{ color: 'white', margin: '15px' }}>Date de iesire:</h3>
        <p style={{ margin: '15px', color: 'white' }}>
          {problem && problem.output}
        </p>
        <h3 style={{ color: 'white', margin: '15px' }}>Restrictii si precizari:</h3>
        <p style={{ margin: '15px', color: 'white' }}>
          {problem && problem.restrictii}
        </p>
        <h3 style={{ color: 'white', margin: '15px' }}>Exemple:</h3>
        {
          problem.exemple.map((exemplu, index) => (
            <div style={{border: '1px solid white', margin: '15px', borderRadius: '7px', fontFamily: 'monospace'}} className="exempluProblema">
              <h2 style={{ color: 'white', margin: '15px'}}>Exemplul {index+1}</h2>
              <h3 style={{ color: 'white', margin: '15px' }}>{problem && (problem.tip === 'fisiere') ? problem.inputFile : 'Input:'}</h3>
              <p style={{ margin: '15px', color: 'white', backgroundColor: '#313131', borderRadius: '7px', textIndent: '15px' }}>{exemplu.input}</p>
              <h3 style={{ color: 'white', margin: '15px' }}>{problem && (problem.tip === 'fisiere') ? problem.outputFile : 'Output:'}</h3>
              <p style={{ margin: '15px', color: 'white', backgroundColor: '#313131', borderRadius: '7px', textIndent: '15px' }}>{exemplu.output}</p>
              {exemplu.explicatie && (
                <>
                  <h3 style={{ color: 'white', margin: '15px', }}>Explicatie:</h3>
                  <p style={{ margin: '15px', color: 'white', backgroundColor: '#313131', borderRadius: '7px', textIndent: '15px', padding: '10px', fontFamily: 'monospace' }}>{exemplu.explicatie}</p></>
              )}
            </div>
          ))
        }
      </div>
      <div className="text-editor" style={{ borderRadius: '5px', border: '1px solid grey'}}>
           <div className="text-editor-options">
              <Select
                  style={{width: '130px', height: '3vh', marginTop: '5px', marginLeft:'15px', color: 'white !important', marginBottom: '10px', border: '1px solid grey', outline: '0px', marginTop: '10px'}}
                  sx={{
                    '& .MuiSelect-icon': {
                      color: 'white'
                    }
                  }}
                  onChange={(e) => {
                    setLanguage(e.target.value)
                    switch(e.target.value){
                      case 'c': {
                        setCode(`#include <stdio.h>

int main() {
    printf("Hello World");
    return 0;
}`)
                      break
                      }
                      case 'cpp': {
                        setCode(`#include <iostream>

int main() {
    std::cout << "Hello World";
    return 0;
}
                            `
                        )
                        break 
                      }
                      case 'cs': {
                        setCode(`using System;

class Program
{
  static void Main()
  {
    Console.WriteLine("Hello, World!");
  }
}
                        `)
                         break
                      }
                      case 'java': {
                      setCode(`class ${titlu} {
    public static void main(String[] args) {
         System.out.println("Hello, World!"); 
  }
}`
                        )
                        break
                      }
                      case 'javascript': {
                      setCode(`console.log(\'Hello world\')`)
                      break
                    }
                      case 'python': {
                        setCode(`print("Hello world")`)
                      }
                    }
                  }}
                  value={language}
                >
                  {languages.map((lang, index) => (
                    <MenuItem key={`lang ${index}`} value={lang.value}>{lang.label}</MenuItem>
                  ))}
                </Select>

                <Select
                  style={{width: '130px', height: '3vh', marginTop: '5px', marginLeft:'15px', color: 'white', marginBottom: '10px', border: '1px solid grey', outline: '0px'}}
                  sx={{
                    '& .MuiSelect-icon': {
                      color: 'white'
                    }
                  }}
                  onChange={(e) => setTheme(e.target.value)}
                  value={theme}
                >
                  {themes.map((theme, index) => (
                    <MenuItem key={`theme ${index}`} value={theme.value}>{theme.label}</MenuItem>
                  ))}
                </Select>
           </div>
        <CodeMirror 
          value={code}
          height="65vh"
          theme={theme}
          extensions={[language === 'python' ? python() : language === 'cpp' ? cpp() : language === 'javascript' ? javascript() : language === 'java' ? java() : python()]}
          inputMode={language}
          onChange={onChangeCode}
          style={{overflowX: 'auto', overflowY: 'auto'}}
          maxWidth="80vw"
        />
        <div>
          <div style={{display: 'flex', marginRight: '10px'}}>
              <>
                <Button
                  variant="contained"
                  color="success"
                  style={{width: '90px', height: '25px', marginTop: '10px', marginLeft: 'auto', marginBottom: '10px'}}
                  endIcon={<IntegrationInstructionsIcon/>}
                  onClick={handleSubmitCode}
                >
                  SUBMIT   
                </Button>
                <Dialog
                  fullScreen={fullScreen}
                  open={open}
                  onClose={handleClose}
                >
                  <DialogTitle>
                    Teste
                  </DialogTitle>
                  <DialogContent>
                    <TableContainer component={Paper} style={{width: 'auto', overflowY: 'auto'}}>
                      <Table>
                        <TableHead>
                          <TableRow>
                            <TableCell align="center">Nr. test</TableCell>
                            <TableCell align="center">Puncte maximum</TableCell>
                            <TableCell align="center">Puncte obtinute</TableCell>
                            <TableCell align="center">Timp executie</TableCell>
                            <TableCell align="center">Mesaj evaluare</TableCell>
                          </TableRow>
                        </TableHead>
                        <TableBody>
                          {testeCompilate && testeCompilate.map((Test, index) => (
                            <TableRow>
                              <TableCell align='center'>{index}</TableCell>
                              <TableCell align="center">{teste[index].points}</TableCell>
                              <TableCell align="center">{Test.puncte}</TableCell>
                              <TableCell align="center">{Test.timpExecutie + 'ms'}</TableCell>
                              <TableCell align="center">+</TableCell>
                            </TableRow>
                          ))}
                        </TableBody>
                      </Table>
                    </TableContainer>
                  </DialogContent>
                  <DialogActions>
                    <Button autoFocus onClick={handleClose} style={{backgroundColor: 'black !important'}}>
                      Quit
                    </Button>
                  </DialogActions>
                </Dialog>
              </>
          </div> 
        </div>
      </div>
      {testeCompilate && testeCompilate[0].error && (
        <div style={{backgroundColor: 'white', width: '70%', marginBottom: '20px', padding: '5px', fontFamily: "monospace", borderRadius: '3px'}} className="erori">
        {'Eroare compilare:'}
          {testeCompilate[0] && (
            testeCompilate[0].error.map((eroare, index) => (
              <div key={index} style={{fontFamily: 'monospace'}}>{eroare}</div>
            ))
          )}
      </div>
      )}

    </div>
  );
};

export default Problem;