import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import Pagination from './pagination';
const Solutii = ({ problema }) => {
  const [page, setPage] = useState(1);
  let [data, setData] = useState();
  let totalPage = data ? Math.ceil(data.length / 30) : 0;
  const stylePoints = (points) => {
    const style = {
       "backgroundColor": ''
    }
    switch(points){
      case 100: 
        style.backgroundColor = 'green'
      break;
      case points >= 70:
        style.backgroundColor = 'yellow'
      break;
      case points >= 40 && points < 70:
        style.backgroundColor = 'orange'
      break
      default: style.backgroundColor = '#C51111'
    }
    return style
  }
  const handlePageChange = (value) => {
    switch (value) {
      case '&laquo;':
      case '...':
        setPage(1);
        break;
      case '&lsaquo;':
        if (page > 1) setPage(page - 1);
        break;
      case '&rsaquo;':
        if (page < totalPage) setPage(page + 1);
        break;
      case '&raquo':
      case '... ':
        setPage(totalPage);
        break;
      default:
        setPage(value);
        break;
    }
  };

  useEffect(() => {
    const fetchData = async () => {
        try {
          const response = await fetch(`https://ic2-backend.onrender.com/problem/getSolutions/${problema}`, { 
            method: 'GET',
            headers: {
              'Content-Type': 'application/json',
            },
          });
          const responseData = await response.json();
          setData(responseData.solutions)
        } catch (error) {
          console.error(error);
        }
      };
      fetchData();
  }, [])
  data =  data && data.slice((page - 1) * 30, page * 30);
  return (
    <div style={{height: '100vh'}}>
    <div className='solutii'>
    <table style={{ color: 'white' }} className='text-center'>
          <thead>
            <tr>
              <th>Username</th>
              <th>Problema</th>
              <th>Limbaj</th>
              <th>Data</th>
              <th>Status</th>
            </tr>
          </thead>
          {data && data.map((solutie) => (
            <tr key={solutie.id}>
              <td>
                <Link to={`/${solutie.username}`}>{solutie.username}</Link>
              </td>
              <td>
                <Link to={`/${solutie.titluProblema}`}>{solutie.titluProblema}</Link>
              </td>
              <td>{solutie.limbaj}</td>
              <td>{solutie.data}</td>
              <td style={stylePoints(solutie.punctaj)}>Evaluat: {solutie.punctaj} puncte</td>
            </tr>
          ))}
        </table>
    </div>
    <div className="pagination" style={{margin: '50px'}}>
        <Pagination totalPage={totalPage} page={page} siblings={1} onPageChange={handlePageChange} />
    </div>
    </div>
  );
};

export default Solutii;
