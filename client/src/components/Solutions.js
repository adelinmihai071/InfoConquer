import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import Pagination from './pagination';

const Solutions = ({ solutii }) => {
  const itemsPerPage = 30;
  const [page, setPage] = useState(1);
  const totalPage = Math.ceil(solutii ? solutii.length / itemsPerPage : 0);
  const startIdx = (page - 1) * itemsPerPage;
  const endIdx = startIdx + itemsPerPage;
  const paginatedSolutii = solutii ? solutii.slice(startIdx, endIdx) : [];

  const stylePoints = (points) => {
    let backgroundColor = '';
    if (points === 100) backgroundColor = 'green';
    else if (points >= 70) backgroundColor = 'yellow';
    else if (points >= 40 && points < 70) backgroundColor = 'orange';
    else backgroundColor = '#C51111';
    return { backgroundColor };
  };

  const handlePageChange = (value) => {
    switch (value) {
      case '&laquo;':
      case '...':
        setPage(1);
        break;
      case '&lsaquo;':
        if (page > 1) setPage(page - 1);
        break;
      case '&rsaquo;':
        if (page < totalPage) setPage(page + 1);
        break;
      case '&raquo':
      case '... ':
        setPage(totalPage);
        break;
      default:
        setPage(value);
        break;
    }
  };

  return (
    <div style={{ height: '100vh' }}>
      <div className="solutii">
        <table style={{ color: 'white' }} className='text-center'>
          <thead>
            <tr>
              <th>Username</th>
              <th>Problema</th>
              <th>Limbaj</th>
              <th>Data</th>
              <th>Status</th>
            </tr>
          </thead>
          <tbody>
            {paginatedSolutii.map((solutie) => (
              <tr key={solutie.id}>
                <td>
                  <Link to={`/${solutie.username}`}>{solutie.username}</Link>
                </td>
                <td>
                  <Link to={`/${solutie.titluProblema}`}>{solutie.titluProblema}</Link>
                </td>
                <td>{solutie.limbaj}</td>
                <td>{solutie.data}</td>
                <td style={stylePoints(solutie.punctaj)}>Evaluat: {solutie.punctaj} puncte</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
      <div className="pagination" style={{margin: '50px'}}>
        <Pagination totalPage={totalPage} page={page} siblings={1} onPageChange={handlePageChange} />
      </div>
    </div>
  );
};

export default Solutions;
